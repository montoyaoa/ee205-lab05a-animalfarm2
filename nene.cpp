///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   10_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {
   Nene::Nene( string newTagID, enum Color newFeatherColor, enum Gender newGender, bool newIsMigratory ) {
      gender         = newGender;
      species        = "Branta sandvicensis";
      featherColor   = newFeatherColor;
      tagID          = newTagID;
      isMigratory    = newIsMigratory;
   }

   void Nene::printInfo() {
      cout << "Nene"                            << endl;
      cout << "   Tag ID = [" << tagID << "]"   << endl;
      Bird::printInfo();
   }

   const string Nene::speak() {
      return string("Nay, nay");
   }
}
